# dots

## server

- websocket server
- has a server state with games currently going on
- game = the grid, participants, score, whose turn is it, state
- receives commands, updates the server state, broadcast state to participants

## game

- create: generate random id
- wait for second player
- starts with empty n by n grid
- player 1 starts, allowed to click any edge
- if the edge created a box, player 1 continues
- else turn advances to player 2
- game ends when all edges have been marked
- winner is the player who created the most amount of boxes
