module Fay.WebSocket where

import FFI

import Fay.Event

data WebSocket
instance Eventable WebSocket

newWebSocket :: String -> Fay WebSocket
newWebSocket = ffi "new WebSocket(%1)"

messageData :: Event -> String
messageData = ffi "%1.data"

send :: WebSocket -> String -> Fay ()
send = ffi "%1.send(%2)"

close :: WebSocket -> Fay ()
close = ffi "%1.close()"
