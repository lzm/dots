module Fay.Event where

import FFI

data Event

class Eventable a

addEventListener :: Eventable a => a -> String -> (Event -> Fay ()) -> Fay ()
addEventListener = ffi "%1[%2] = %3"
