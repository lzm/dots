{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Control.Monad (unless)
import Control.Exception
import qualified Data.Text          as T
import qualified Network.WebSockets as WS

import Dots.Server

loop :: Server -> WS.Connection -> IO ()
loop server conn = do
  msg <- WS.receiveData conn
  putStrLn $ T.unpack msg
  WS.sendTextData conn $ T.append "received: " msg
  unless (msg == "bye") $ loop server conn

disconnect :: IO ()
disconnect = do
  putStrLn "someone disconnected"

main :: IO ()
main = do
  let host = "0.0.0.0"
      port = 40444
      server = emptyServer

  putStrLn $ "Server listening at " ++ host ++ ":" ++ show port

  WS.runServer host port $ \pending -> do
    conn <- WS.acceptRequest pending
    WS.forkPingThread conn 30
    putStrLn "someone connected"
    finally (loop server conn) disconnect `catch` ignore

ignore :: SomeException -> IO ()
ignore _ = return ()
