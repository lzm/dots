module Client (main) where

import Fay.Event
import Fay.WebSocket

onOpen :: WebSocket -> Event -> Fay ()
onOpen conn e = do
  print e
  send conn "hi"

onMessage :: WebSocket -> Event -> Fay ()
onMessage conn e = do
  print e

main :: Fay ()
main = do
  conn <- newWebSocket "ws://localhost:40444"
  addEventListener conn "onopen" $ onOpen conn
  addEventListener conn "onmessage" $ onMessage conn
