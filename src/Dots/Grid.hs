module Dots.Grid where

import Prelude hiding (lines)

import Data.Maybe

import Dots.Util

data Grid = Grid
  { width  :: Int
  , height :: Int
  , lines  :: [[Bool]]
  , boxes  :: [[Maybe Player]]
  } deriving (Show)

type Line   = (Int, Int)
type Player = Int

generate :: Int -> Int -> Grid
generate w h = Grid
  { width  = w
  , height = h
  , lines = [replicate (w + y `mod` 2) False | y <- [0..h*2-1]]
  , boxes = replicate h $ replicate w Nothing
  }

markLine :: Player -> Line -> Grid -> (Grid, Bool)
markLine player (y, x) grid = (newGrid, madeBox)
  where
    madeBox    = boxes grid /= newBoxes
    newGrid    = grid { lines = newLines, boxes = newBoxes }
    newLines   = change y (set x True) $ lines grid
    newBoxes   = [[newBox y x | x <- [0..width grid-1]] | y <- [0..height grid-1]]
    newBox y x = if isJust $ oldBox y x
                 then oldBox y x
                 else if boxed y x
                      then Just player
                      else Nothing
    -- is this box enclosed by lines?
    boxed  y x = and $ map (uncurry line) [(y*2, x), (y*2+1, x), (y*2+1, x+1), (y*2+2, x)]
    line   y x = at y x newLines
    oldBox y x = at y x $ boxes grid
