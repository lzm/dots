module Dots.Server where

import Dots.Game

data Server = Server
  { games :: [Game]
  }

emptyServer :: Server
emptyServer = Server []
