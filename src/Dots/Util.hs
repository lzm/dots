module Dots.Util where

change :: Int -> (a -> a) -> [a] -> [a]
change n f xs = take n xs ++ [f $ xs!!n] ++ drop (n + 1) xs

set :: Int -> a -> [a] -> [a]
set n x = change n $ const x

at :: Int -> Int -> [[a]] -> a
at y x xs = xs!!y!!x
