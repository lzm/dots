module Dots.Game where

import Data.Text

import Dots.Grid

data State
  = WaitingForPlayer
  | PlayerTurn
  | GameOver

data Game = Game
  { name  :: Text
  , turn  :: Player
  , grid  :: Grid
  , state :: State
  }
