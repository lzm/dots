all: client

client:
	fay src/Client.hs --output www/dots.js --include src --pretty

server:
	cabal build
	dist/build/dots/dots
